# Aplicación agenda #

Esta app es un ejemplo de uso de la librería sqliblite. Las operaciones que realiza son:

* Consulta datos de la base de datos
* Inserta nuevos datos en la base de datos
* Actualiza datos de la base de datos
* Elimina datos de la base de datos

Las operaciones de editar y eliminar están metidas dentro de un menú contextual en la lista. 