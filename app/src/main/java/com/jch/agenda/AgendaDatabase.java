package com.jch.agenda;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.jch.sqliblite.data.AbstractDatabase;
import com.jch.sqliblite.statements.ddl.Colum;
import com.jch.sqliblite.statements.ddl.Constraint;
import com.jch.sqliblite.statements.ddl.CreateTable;
import com.jch.sqliblite.statements.ddl.DropTable;
import com.jch.sqliblite.statements.ddl.Type;

/**
 *
 * Created by javier on 22/06/15.
 */
public class AgendaDatabase extends AbstractDatabase {

    private static final String NOMBRE_BD = "agenda_bd";
    private static final int VERSION_BD = 1;

    public AgendaDatabase(Context ctx) {
        super(ctx);
    }

    @Override
    public String getName() {
        return NOMBRE_BD;
    }

    @Override
    public int getVersion() {
        return VERSION_BD;
    }

    @Override
    protected void executeCreate(SQLiteDatabase sqLiteDatabase) {
        String sql = CreateTable.name(TablaContactos.TABLA).colums(
                Colum.value(TablaContactos._ID, Type.INTEGER, Constraint.PRIMARY_KEY_AUTOINCREMENT),
                Colum.value(TablaContactos.NOMBRE, Type.VARCHAR, Constraint.NOT_NULL),
                Colum.value(TablaContactos.DIRECCION, Type.VARCHAR, Constraint.NOT_NULL),
                Colum.value(TablaContactos.TELEFONO, Type.INTEGER, Constraint.NOT_NULL))
                    .builStatement();
        sqLiteDatabase.execSQL(sql);
    }

    @Override
    protected void executeUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        String sql = DropTable.drop(TablaContactos.TABLA);
        sqLiteDatabase.execSQL(sql);
    }
}
