package com.jch.agenda;

import android.database.Cursor;

import com.jch.sqliblite.data.EntityMapper;

/**
 * Created by javier on 22/06/15.
 */
public class PersonaMapper implements EntityMapper<Persona> {

    @Override
    public Persona mapEntity(Cursor cursor) {
        Persona persona = new Persona();
        persona.setId(cursor.getInt(cursor.getColumnIndex(TablaContactos._ID)));
        persona.setNombre(cursor.getString(cursor.getColumnIndex(TablaContactos.NOMBRE)));
        persona.setDireccion(cursor.getString(cursor.getColumnIndex(TablaContactos.DIRECCION)));
        persona.setTelefono(cursor.getInt(cursor.getColumnIndex(TablaContactos.TELEFONO)));
        return persona;
    }
}
