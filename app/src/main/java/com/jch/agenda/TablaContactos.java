package com.jch.agenda;

import android.provider.BaseColumns;

/**
 *
 * Created by javier on 22/06/15.
 */
public class TablaContactos implements BaseColumns {

    public static final String TABLA = "contactos";

    public static final String NOMBRE = "nombre";
    public static final String DIRECCION = "direccion";
    public static final String TELEFONO = "telefono";

}
