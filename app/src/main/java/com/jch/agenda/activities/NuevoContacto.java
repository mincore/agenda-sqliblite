package com.jch.agenda.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jch.agenda.MainActivity;
import com.jch.agenda.R;
import com.jch.agenda.TablaContactos;
import com.jch.agenda.util.Utilidades;
import com.jch.sqliblite.statements.Insert;

public class NuevoContacto extends AppCompatActivity {

    private static final String TAG = "NuevoContacto";

    private EditText mNombre;
    private EditText mDireccion;
    private EditText mTelefono;
    private Button mInsertar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_contacto);
        mNombre = (EditText) findViewById(R.id.nombre_contacto);
        mDireccion = (EditText) findViewById(R.id.direccion_contacto);
        mTelefono = (EditText) findViewById(R.id.telefono_contacto);
        mInsertar = (Button) findViewById(R.id.boton_insertar);
        mInsertar.setOnClickListener(onClickListener);
    }

    private final View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String nombre = mNombre.getText().toString();
            String direccion = mDireccion.getText().toString();
            String telefono = mTelefono.getText().toString();
            if(nombre.isEmpty() || direccion.isEmpty() || telefono.isEmpty()) {
                Toast.makeText(getApplicationContext(), "Rellena todos los campos",
                        Toast.LENGTH_LONG).show();
                return;
            }
            if(!Utilidades.validarTelefono(telefono)) {
                Toast.makeText(getApplicationContext(), "Numero de telefono no valido",
                        Toast.LENGTH_LONG).show();
                return;
            }
            Integer tlfno = Integer.valueOf(telefono);
            String sql = new Insert().insert(TablaContactos.TABLA).values(null, nombre, direccion, tlfno)
                    .builStatement();
            try {
                long registro = MainActivity.dataOperation.insert(sql);
                Log.i(TAG, "Insertado nuevo contacto con ID " + registro);
                Toast.makeText(getApplicationContext(), "Insertado nuevo contacto!", Toast.LENGTH_LONG).show();
            } catch (RuntimeException e) {
                Log.w(TAG, "Imposible insertar el contacto");
            }
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nuevo_contacto, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
