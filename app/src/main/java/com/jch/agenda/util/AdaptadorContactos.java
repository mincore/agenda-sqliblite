package com.jch.agenda.util;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jch.agenda.Persona;
import com.jch.agenda.R;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Created by javier on 22/06/15.
 */
public class AdaptadorContactos extends BaseAdapter {

    private List<Persona> mPersonas;

    /**
     * Constructor
     *
     * @param mPersonas
     */
    public AdaptadorContactos(List<Persona> mPersonas) {
        this.mPersonas = mPersonas;
    }

    public void actualizarContactos(List<Persona> personas) {
        mPersonas = personas;
    }

    @Override
    public int getCount() {
        return mPersonas.size();
    }

    @Override
    public Object getItem(int i) {
        return mPersonas.get(i);
    }

    @Override
    public long getItemId(int i) {
        return mPersonas.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view == null) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.info_contacto, viewGroup, false);
        }
        TextView nombre = (TextView) view.findViewById(R.id.nombre);
        TextView direccion = (TextView) view.findViewById(R.id.direccion);
        TextView telefono = (TextView) view.findViewById(R.id.telefono);
        Persona persona = mPersonas.get(i);
        nombre.setText(persona.getNombre());
        direccion.setText(persona.getDireccion());
        telefono.setText(String.valueOf(persona.getTelefono()));
        return view;
    }
}
