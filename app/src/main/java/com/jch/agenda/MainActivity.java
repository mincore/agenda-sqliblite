package com.jch.agenda;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.jch.agenda.activities.EditarContacto;
import com.jch.agenda.activities.NuevoContacto;
import com.jch.agenda.util.AdaptadorContactos;
import com.jch.sqliblite.data.DataOperation;
import com.jch.sqliblite.functions.Comparison;
import com.jch.sqliblite.statements.Delete;
import com.jch.sqliblite.statements.Select;

import java.util.List;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    // Operaciones en la base de datos
    public static DataOperation dataOperation;

    private ListView mListaContactos;
    private AdaptadorContactos mAdapterContactos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final AgendaDatabase agendaDatabase = new AgendaDatabase(getApplicationContext());
        dataOperation = new DataOperation(agendaDatabase);

        final List<Persona> contactos = cargarContactos();
        if(contactos.isEmpty()) {
            Log.i(TAG, "No hay contactos, enviando a pantalla para crear uno");
            nuevoContacto();
        }
        Log.i(TAG, "Recuperados " + contactos.size() + " contactos");
        mListaContactos = (ListView) findViewById(R.id.lista_contactos);
        mAdapterContactos = new AdaptadorContactos(contactos);
        mListaContactos.setAdapter(mAdapterContactos);

        registerForContextMenu(mListaContactos);
    }

    @Override
    protected void onResume() {
        super.onResume();
        actualizar();
    }

    private void nuevoContacto() {
        Intent nuevoContacto = new Intent(getApplicationContext(), NuevoContacto.class);
        startActivity(nuevoContacto);
    }

    public static List<Persona> cargarContactos() {
        String sql = new Select().select().from(TablaContactos.TABLA).builStatement();
        return dataOperation.get(sql, new PersonaMapper());
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater menuInflater = new MenuInflater(getApplicationContext());
        menuInflater.inflate(R.menu.menu_contextual, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.menu_editar:
                int posicionEditar = menuInfo.position;
                Log.i(TAG, "Seleccionada la posision " + posicionEditar + " para editar");
                Persona contactoEditar = (Persona) mAdapterContactos.getItem(posicionEditar);
                modificarContacto(contactoEditar);
                return true;
            case R.id.menu_eliminar:
                int posicionEliminar = menuInfo.position;
                Persona contacto = (Persona) mAdapterContactos.getItem(posicionEliminar);
                Log.i(TAG, "Seleccionada la posision " + posicionEliminar + " para eliminar");
                Log.i(TAG, "Preparando para eliminar contacto " + contacto.getNombre());
                int registroEliminado = eliminarContacto(contacto);
                Log.i(TAG, "Eliminado registro " + registroEliminado + " de la base de datos");
                actualizar();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private int eliminarContacto(Persona persona) {
        String sql = new Delete().delete(TablaContactos.TABLA).where(TablaContactos._ID,
                Comparison.equal(persona.getId())).builStatement();
        return dataOperation.delete(sql);
    }

    private void actualizar() {
        List<Persona> personas = cargarContactos();
        mAdapterContactos.actualizarContactos(personas);
        ((BaseAdapter)mListaContactos.getAdapter()).notifyDataSetChanged();
    }

    private void modificarContacto(Persona persona) {
        Intent actualizar = new Intent(getApplicationContext(), EditarContacto.class);
        actualizar.putExtra("PERSONA", persona);
        startActivity(actualizar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.nuevo_contacto) {
            nuevoContacto();
        }

        return super.onOptionsItemSelected(item);
    }
}
